FROM rockylinux:8 AS builder

WORKDIR /tmp

RUN dnf install -y epel-release && dnf install -y bzip2 wget bash autoconf automake gcc gcc-c++ libtool valgrind-devel gmp-devel make pkgconf-pkg-config git zeromq-devel

RUN wget https://raw.githubusercontent.com/libbitcoin/libbitcoin-server/version3/install.sh -O install.sh && chmod 755 install.sh

RUN CXX=g++ CFLAGS='-Os -s' CXXFLAGS='-Os -s' ./install.sh --prefix=/tmp --build-dir=/tmp --build-boost --disable-shared

FROM rockylinux:8

RUN dnf install -y epel-release && dnf install -y zeromq-devel

WORKDIR /data
RUN mkdir /etc/bs

COPY --from=builder /tmp/bin/bs /root/
COPY entrypoint.sh /
COPY bs.cfg /etc/bs/

EXPOSE 9091 8333

ENTRYPOINT ["/entrypoint.sh"]
