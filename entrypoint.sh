#!/bin/bash

DIR="/data/blockchain"

cd /data

if [ -d "$DIR" ]; then
  echo "################ Starting Bitcoin Server... #####################"
  rm -rf $DIR/flush_lock
  ./bs --config /etc/bs/bs.cfg
else
  echo "##################### First Run, Initialising Blockchain... #####################"
  ./bs --initchain
  echo "##################### Starting Bitcoin Server... #####################"
  rm -rf $DIR/flush_lock
  ./bs --config /etc/bs/bs.cfg 
fi
